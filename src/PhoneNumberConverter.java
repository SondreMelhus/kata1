import java.util.HashMap;
import java.util.*;

public class PhoneNumberConverter {

    //Static method used to convert a phone number containing text, into a phone number only containing numbers
    public static void textToNum(String phoneNr, HashMap<Character, Integer> numberMap) {
        String convertedNumber = "";
        char [] number = phoneNr.toCharArray();         //Converting phoneNr parameter into an array of chars

        for (char character : number) {                 //Looping through the str array
            if (numberMap.get(character) != null) {     //Checking if the hashmap contians the given key
                convertedNumber = convertedNumber + numberMap.get(character);   //Concating the char value from the hashmap to the end of convertedNumber
            } else {
                convertedNumber = convertedNumber + character;      //Concating the character to the end of convertedNumber if it is not contained in the hashmap
            }
        }
        System.out.println(convertedNumber);
    }

    public static void main(String[] args) {
        String phoneNr;
        HashMap <Character, Integer> numberMap = new HashMap<Character, Integer>();

        numberMap.put(' ', 0);
        numberMap.put(' ', 1);

        numberMap.put('A', 2);
        numberMap.put('B', 2);
        numberMap.put('C', 2);

        numberMap.put('D', 3);
        numberMap.put('E', 3);
        numberMap.put('F', 3);

        numberMap.put('G', 4);
        numberMap.put('H', 4);
        numberMap.put('I', 4);

        numberMap.put('J', 5);
        numberMap.put('K', 5);
        numberMap.put('L', 5);

        numberMap.put('M', 6);
        numberMap.put('N', 6);
        numberMap.put('O', 6);

        numberMap.put('P', 7);
        numberMap.put('Q', 7);
        numberMap.put('R', 7);
        numberMap.put('S', 7);

        numberMap.put('T', 8);
        numberMap.put('U', 8);
        numberMap.put('V', 8);

        numberMap.put('W', 9);
        numberMap.put('X', 9);
        numberMap.put('Y', 9);
        numberMap.put('Z', 9);

        String number1 = "123-647-EYES";
        String number2 = "(325)444-TEST";
        String number3 = "653-TRY-THIS";
        String number4 = "435-224-7613";

        PhoneNumberConverter.textToNum(number1, numberMap);
        PhoneNumberConverter.textToNum(number2, numberMap);
        PhoneNumberConverter.textToNum(number3, numberMap);
        PhoneNumberConverter.textToNum(number4, numberMap);

    }
}
